package com.virginpulse.MovieApp;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * create an instance of this fragment.
 */
public class ListFrag extends Fragment {

    View view;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter adapter;
    ArrayList<Movie> moviesList;

    public ListFrag() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        recyclerView = view.findViewById(R.id.movList);
        layoutManager = new LinearLayoutManager(this.getContext());
        recyclerView.setLayoutManager(layoutManager);
        moviesList =  new ArrayList<>();
        adapter = new MovieAdapter(this.getContext(), moviesList);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_list, container, false);
        return view;
    }

    public void setRecyclerViewAdapter(ArrayList<Movie> list){
        moviesList.addAll(list);
        adapter.notifyDataSetChanged();
    }

    public void setRecyclerViewAdapterClear(ArrayList<Movie> list){
        moviesList.clear();
        moviesList.addAll(list);
        adapter = new MovieAdapter(this.getContext(), moviesList);
        recyclerView.setAdapter(adapter);
    }

    public RecyclerView getRecyclerView(){
        return recyclerView;
    }

    public RecyclerView.Adapter getAdapter(){
        return adapter;
    }
}