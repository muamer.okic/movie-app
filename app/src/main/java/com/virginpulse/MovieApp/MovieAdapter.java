package com.virginpulse.MovieApp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ViewHolder>{

    private ArrayList<Movie> movList;
    Context context;


    public MovieAdapter(Context context, ArrayList<Movie> list) {
        movList = list;
        this.context = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ivPoster;
        TextView txtName;
        TextView txtRelDate;
        TextView txtDesc;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivPoster = (ImageView) itemView.findViewById(R.id.ivPoster);
            txtName = itemView.findViewById(R.id.txtName);
            txtRelDate = itemView.findViewById(R.id.txtRelDate);
            txtDesc = itemView.findViewById(R.id.txtDesc);


        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.itemView.setTag(movList.get(position));
        holder.txtName.setText(movList.get(position).getTitle());

        Picasso p = Picasso.get();
        p.load("https://image.tmdb.org/t/p/w92" + movList.get(position).getPosterPath()).into(holder.ivPoster);
        holder.txtRelDate.setText(movList.get(position).getRelease_date());
        holder.txtDesc.setText(movList.get(position).getOverview());
    }

    @Override
    public int getItemCount() {
        return movList.size();
    }

}
