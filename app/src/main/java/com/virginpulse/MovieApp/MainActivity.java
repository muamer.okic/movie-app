package com.virginpulse.MovieApp;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    SearchView movSearch;
    FragmentManager fragmentManager;
    LinearLayout searchLayout;
    ArrayAdapter<String> adapter;
    ListView lvMovies;
    ArrayList<String> list;
    MovieDB database;
    int pageNum;
    ListFrag listFrag;
    int CurrPage;
    String CurrQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        movSearch = findViewById(R.id.movSearch);
        database = new MovieDB(this);
        fragmentManager = this.getSupportFragmentManager();
        searchLayout = findViewById(R.id.searchLayout);
        listFrag = (ListFrag) fragmentManager.findFragmentById(R.id.listFrag);
        lvMovies = findViewById(R.id.lvMovies);
        fragmentManager.beginTransaction().hide(fragmentManager.findFragmentById(R.id.listFrag)).commit();
        lvMovies.setVisibility(View.GONE);

        lvMovies = findViewById(R.id.lvMovies);
        list = new ArrayList<>();
        database.open();
        database.readDB(list);
        database.close();
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list);
        lvMovies.setAdapter(adapter);

        movSearch.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b)
                lvMovies.setVisibility(View.VISIBLE);
                else
                    lvMovies.setVisibility(View.GONE);
            }
        });

        movSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                apiCall(s,1, false);
                setScrollListener();


                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                adapter.getFilter().filter(s);
                return false;
            }
        });

        lvMovies.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                apiCall(adapter.getItem(i), 1, false);
                setScrollListener();
            }
        });






    }

    @Override
    public void onBackPressed() {
        if(searchLayout.getVisibility() == View.VISIBLE)
        super.onBackPressed();
        else {
            fragmentManager.beginTransaction().hide(fragmentManager.findFragmentById(R.id.listFrag)).commit();
            searchLayout.setVisibility(View.VISIBLE);
            lvMovies.setVisibility(View.GONE);
            movSearch.setQuery("", false);

        }
    }


    public void setAdapter() {
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list);
        lvMovies.setAdapter(adapter);
    }

    public void showToast(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    public void apiCall(final String query, final Integer page, final boolean newPage) {
        CurrQuery = query;
        CurrPage = page;
        String api_key = "2696829a81b1b5827d515ff121700838";
        OkHttpClient client = new OkHttpClient();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.themoviedb.org/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        MovieEndPoints request = retrofit.create(MovieEndPoints.class);
        Call<MoviesResponse> call = request.getMovies(api_key, query, page.toString());

        call.enqueue(new Callback<MoviesResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(Call<MoviesResponse> call, Response<MoviesResponse> response) {
                if(response.body().getResults().size() == 0)
                {
                    showToast("No results");
                    return;
                }
                ArrayList<Movie> movies = (ArrayList<Movie>) response.body().getResults();
                pageNum = response.body().getTotalPages();
                if(newPage)
                listFrag.setRecyclerViewAdapter(movies);
                else {
                    listFrag.setRecyclerViewAdapterClear(movies);
                }

                searchLayout.setVisibility(View.GONE);
                fragmentManager.beginTransaction().show(fragmentManager.findFragmentById(R.id.listFrag)).commit();
                database.open();
                boolean searched = false;
                for(String e : list) {
                    if(e.toLowerCase().compareTo(query.toLowerCase()) == 0) {
                        searched = true;
                        break;
                    }
                }
                if(!searched) {
                    if(list.size() == 10) {
                        database.addRow(query);
                        database.deleteFirst();
                        list.remove(list.size()-1);
                    }
                    else
                        database.addRow(query);
                    list.add(0,query);
                    setAdapter();
                }


            }
            @Override
            public void onFailure(Call<MoviesResponse> call, Throwable t) {
                showToast(t.getMessage());
            }
        });
    }

    public void setScrollListener() {
        RecyclerView recView = listFrag.getRecyclerView();
        recView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (!recyclerView.canScrollVertically(1)) {
                    if(CurrPage < pageNum){
                        CurrPage++;
                        showToast("Loading");
                        apiCall(CurrQuery, CurrPage, true);
                    }

                }
            }
        });
    }

    public void setAdapterBottomListener(){

    }


}