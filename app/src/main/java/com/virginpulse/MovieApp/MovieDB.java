package com.virginpulse.MovieApp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class MovieDB {

    public static final String COL_ID = "_id";
    public static final String COL_SEARCH = "_search";

    private final String DB_NAME = "MovieDB";
    private final String DB_TABLE = "search";
    private final int DB_VERSION = 1;

    private DBHelper dbhelper;
    private final Context dbContext;
    private SQLiteDatabase database;

    public MovieDB (Context context) {
        dbContext = context;
    }

    private class DBHelper extends SQLiteOpenHelper {

        public DBHelper (Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {

            String sqlQuery = "CREATE TABLE " + DB_TABLE + " (" + COL_ID +
                    " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL_SEARCH +
                    " TEXT NOT NULL);";
            db.execSQL(sqlQuery);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + DB_TABLE);
            onCreate(db);
        }
    }


    public MovieDB open() throws SQLException {
        dbhelper = new DBHelper(dbContext);
        database = dbhelper.getWritableDatabase();
        return this;
    }

    public void close() {
        dbhelper.close();
    }


    public long addRow(String s) {
        ContentValues content = new ContentValues();
        content.put(COL_SEARCH, s);
        return database.insert(DB_TABLE, null, content);
    }

    public void deleteFirst(){
        String[] col = new String[] {COL_ID, COL_SEARCH};
        Cursor cursor = database.query(DB_TABLE, col, null, null, null, null, null);
        int idInd = cursor.getColumnIndex(COL_ID);
        cursor.moveToFirst();
        int id = cursor.getInt(idInd);
        cursor.close();
        String query = "DELETE FROM " + DB_TABLE + " WHERE " + COL_ID + " = " + id + ";";
        database.execSQL(query);
    }

    public void readDB(ArrayList<String> list) {
        list.clear();
        String[] col = new String[] {COL_ID, COL_SEARCH};
        Cursor cursor = database.query(DB_TABLE, col, null, null, null, null, null);
        int searchInd = cursor.getColumnIndex(COL_SEARCH);
        cursor.moveToLast();
        for(; !cursor.isBeforeFirst(); cursor.moveToPrevious()) {
            list.add(cursor.getString(searchInd));
        }
        cursor.close();
    }

}
